<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "lineas".
 *
 * @property int $id
 * @property int|null $producto
 * @property string|null $nombreProducto
 * @property float|null $precio
 * @property int|null $cantidad
 * @property float|null $total
 * @property int|null $factura
 *
 * @property Factura $factura0
 * @property Productos $producto0
 */
class Lineas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lineas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['producto', 'cantidad', 'factura'], 'integer'],
            [['precio', 'total'], 'number'],
            [['nombreProducto'], 'string', 'max' => 100],
            [['factura'], 'exist', 'skipOnError' => true, 'targetClass' => Factura::class, 'targetAttribute' => ['factura' => 'id']],
            [['producto'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::class, 'targetAttribute' => ['producto' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'producto' => 'Producto',
            'nombreProducto' => 'Nombre Producto',
            'precio' => 'Precio',
            'cantidad' => 'Cantidad',
            'total' => 'Total',
            'factura' => 'Factura',
        ];
    }

    /**
     * Gets query for [[Factura0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFactura0()
    {
        return $this->hasOne(Factura::class, ['id' => 'factura']);
    }

    /**
     * Gets query for [[Producto0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProducto0()
    {
        return $this->hasOne(Productos::class, ['id' => 'producto']);
    }

    public function getProductos()
    {
        $secciones = Productos::find()->all();
        return ArrayHelper::map($secciones, 'id', 'nombre');
    }

    public function getFacturas()
    {
        $secciones = Factura::find()->all();
        return ArrayHelper::map($secciones, 'id', 'id');
    }
}
