<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "productos".
 *
 * @property int $id
 * @property string|null $nombre
 * @property float|null $precio
 * @property int|null $grupo
 * @property string|null $nombreGrupo
 * @property float|null $ivaGrupo
 *
 * @property Grupos $grupo0
 * @property Lineas[] $lineas
 */
class Productos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['precio', 'ivaGrupo'], 'number'],
            [['grupo'], 'integer'],
            [['nombre', 'nombreGrupo'], 'string', 'max' => 100],
            [['grupo'], 'exist', 'skipOnError' => true, 'targetClass' => Grupos::class, 'targetAttribute' => ['grupo' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'precio' => 'Precio',
            'grupo' => 'Grupo',
            'nombreGrupo' => 'Nombre Grupo',
            'ivaGrupo' => 'Iva Grupo',
        ];
    }

    /**
     * Gets query for [[Grupo0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGrupo0()
    {
        return $this->hasOne(Grupos::class, ['id' => 'grupo']);
    }

    /**
     * Gets query for [[Lineas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLineas()
    {
        return $this->hasMany(Lineas::class, ['producto' => 'id']);
    }

    public function getGrupos()
    {
        $salida = Grupos::find()->all();
        return ArrayHelper::map($salida, 'id', 'nombre');
    }
}
