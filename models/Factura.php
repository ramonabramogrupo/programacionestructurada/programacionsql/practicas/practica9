<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "factura".
 *
 * @property int $id
 * @property string|null $cliente
 * @property string|null $telefono
 * @property string|null $correo
 * @property float|null $total
 * @property float|null $iva
 * @property float|null $totalIva
 *
 * @property Lineas[] $lineas
 */
class Factura extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'factura';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['total', 'iva', 'totalIva'], 'number'],
            [['cliente', 'telefono', 'correo'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cliente' => 'Cliente',
            'telefono' => 'Telefono',
            'correo' => 'Correo',
            'total' => 'Total',
            'iva' => 'Iva',
            'totalIva' => 'Total Iva',
        ];
    }

    /**
     * Gets query for [[Lineas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLineas()
    {
        return $this->hasMany(Lineas::class, ['factura' => 'id']);
    }
}
