<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Grupos $model */

$this->title = 'Create Grupos';
$this->params['breadcrumbs'][] = ['label' => 'Grupos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grupos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
