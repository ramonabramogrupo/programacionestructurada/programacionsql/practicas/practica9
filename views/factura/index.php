<?php

use app\models\Factura;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Facturas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="factura-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Factura', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Actualizar Datos', ['actualizar'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'cliente',
            'telefono',
            'correo',
            'total',
            'iva',
            'totalIva',
            [
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="far fa-person-sign"></i>Actualizar Datos Factura', ['factura/actualizarfactura', 'id' => $model->id], ['class' => 'btn btn-secondary']);
                }
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Factura $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                }
            ],
        ],
    ]); ?>


</div>