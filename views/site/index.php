<?php

/** @var yii\web\View $this */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent mt-5 mb-5">
        <h1 class="display-4">Practica programacion sql</h1>

    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4 mb-3">
                <h2>Procedimientos</h2>
            </div>
            <div class="col-lg-4 mb-3">
                <h2>Funciones</h2>

            </div>
            <div class="col-lg-4">
                <h2>Disparadores</h2>
            </div>
        </div>

    </div>
</div>