<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Lineas $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="lineas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'producto')->dropDownList($model->productos) ?>

    <?= $form->field($model, 'nombreProducto')->textInput(['disabled' => true]) ?>

    <?= $form->field($model, 'precio')->textInput(['disabled' => true]) ?>

    <?= $form->field($model, 'cantidad')->input('number') ?>

    <?= $form->field($model, 'total')->textInput(['disabled' => true]) ?>

    <?= $form->field($model, 'factura')->dropDownList($model->facturas) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>