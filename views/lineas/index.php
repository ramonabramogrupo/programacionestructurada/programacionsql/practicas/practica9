<?php

use app\models\Lineas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Lineas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lineas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Nuevo', ['lineas/create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Actualizar Datos', ['lineas/actualizar'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'producto',
            'nombreProducto',
            'precio',
            'cantidad',
            'total',
            'factura',
            [
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="far fa-person-sign"></i>Actualizar Datos Linea', ['lineas/actualizarlinea', 'id' => $model->id], ['class' => 'btn btn-secondary']);
                }
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Lineas $model, $key, $index, $column) {
                    return Url::toRoute(["lineas/$action", 'id' => $model->id]);
                }
            ],
        ],
    ]); ?>


</div>