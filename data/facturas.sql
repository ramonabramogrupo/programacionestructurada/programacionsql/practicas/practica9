﻿DROP DATABASE IF EXISTS facturas;
CREATE DATABASE facturas;
USE facturas;

CREATE TABLE factura(
  id int AUTO_INCREMENT,
  cliente varchar(100),
  telefono varchar(100),
  correo varchar(100),
  total float,
  iva float,
  totalIva float,
  PRIMARY KEY(id)
);

CREATE TABLE grupos(
id int AUTO_INCREMENT,
nombre varchar(100),
iva float,
PRIMARY KEY(id)
);

CREATE TABLE productos(
id int AUTO_INCREMENT,
nombre varchar(100),
precio float,
grupo int,
nombreGrupo varchar(100),
ivaGrupo float,
PRIMARY KEY(id),
CONSTRAINT fkproductosGrupos FOREIGN KEY (grupo) REFERENCES grupos(id) on DELETE CASCADE on UPDATE CASCADE
);

CREATE TABLE lineas(
id int AUTO_INCREMENT,
producto int,
nombreProducto varchar(100),
precio float,
cantidad int,
total float,
factura int,
PRIMARY KEY(id),
CONSTRAINT fklineasFacturas FOREIGN KEY (factura) REFERENCES factura(id) on DELETE CASCADE on UPDATE CASCADE,
CONSTRAINT fklineasProductos FOREIGN KEY (producto) REFERENCES productos(id) on DELETE CASCADE on UPDATE CASCADE
);

